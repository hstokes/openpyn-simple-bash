#!/usr/bin/env bash

readonly PROJECT_PATH="/home/${USER}/Documents/Gitlab Projects/openpyn-simple-bash/"
readonly ARGUMENTS=("${@}")

load_packages() {
  for package in "notification" "timing" "openpyn_status" "connection"; do

    # shellcheck source=/dev/null
    source "${PROJECT_PATH}${package}.sh"
  done
}

no_argument_passed() {

  [[ ${#} -lt 1 ]]
}

wrong_country_name() {

  # shellcheck source=/dev/null
  [[ $(source "${PROJECT_PATH}country.sh" "${1}") == "unknown" ]]
}

is_connected() {

  local -r RESPONSE=$(openpyn_status::get_status)

  [[ "${RESPONSE}" == *"active (running)"* && "${RESPONSE}" == *"Initialization Sequence Completed"* ]]
}

should_move_to_another_location() {

  # shellcheck source=/dev/null
  local -r EXPECTED_COUNTRY=$(source "${PROJECT_PATH}country.sh" "${1}")
  local -r ACTUAL_COUNTRY=$(get_country_name)
  local -r EXPECTED_AREA=$(get_area_from_argument "${ARGUMENTS[@]}")
  local -r ACTUAL_AREA=$(get_area_name)

  if [[ ${ACTUAL_COUNTRY} != "unknown" && ${ACTUAL_COUNTRY} != "${EXPECTED_COUNTRY}" ]]; then
    notification::notify "Moving from ${ACTUAL_COUNTRY} to ${EXPECTED_COUNTRY}."

    return 0
  elif [[ ${ACTUAL_AREA} != "${EXPECTED_AREA}" ]]; then
    notification::notify "Moving from ${ACTUAL_AREA} to ${EXPECTED_AREA}."

    return 0
  fi

  return 1
}

get_country_name() {

  local -r COUNTRY_SHORTCUT=$(openpyn_status::get_status | grep -o -P '(?<=ovpn_udp/).*(?=.nordvpn)' | awk '{print $1; exit}' | sed 's/[0-9]//g')

  # shellcheck source=/dev/null
  "${PROJECT_PATH}country.sh" "${COUNTRY_SHORTCUT}"
}

get_area_from_argument() {

  local area="unknown"

  while [ "${1}" != "" ]; do
    case ${1} in
    -a | --area)
      area="${2//\'/}"
      ;;
    esac
    shift
  done

  echo "${area}"
}

get_area_name() {

  local -r AREA=$(openpyn_status::get_status | grep -o -P '(?<=--area ).*(?= --max-load)')

  if [[ ${#AREA} -lt 1 ]]; then
    echo "unknown"
  else
    echo "${AREA//\'/}"
  fi
}

is_connection_down() {

  local -r RESPONSE=$(openpyn_status::get_status)

  [[ "${RESPONSE}" == *"Active: inactive (dead)"* || "${RESPONSE}" == *"Active: failed"* ]]
}

daemon_started() {

  openpyn_status::status_contains "active (running)"
}

received_error() {

  local -r ERROR_MESSAGE=$(get_error_message)

  [[ ${#ERROR_MESSAGE} -gt 0 ]]
}

get_error_message() {

  openpyn_status::get_status | grep -o "\[ERROR\].*"
}

authentication_failed() {

  openpyn_status::status_contains "AUTH_FAILED"
}

main() {
  load_packages

  if no_argument_passed "${@}"; then
    notification::error "No argument has been passed."

    # bad argument
    exit 128
  elif wrong_country_name "${1}"; then
    notification::error "Wrong country name."

    exit 128
  fi

  if is_connected; then
    if should_move_to_another_location "${1}"; then
      openpyn -x >/dev/null 2>&1

      if ! timing::wait_condition is_connection_down 8000; then
        notification::error "Failed to disconnect."

        exit 1
      fi
    else
      notification::notify "Already connected to $(get_area_name)." "$(get_country_name)." "IP: $(connection::get_ip)."

      exit 0
    fi
  fi

  # -f forces network to only use VPN connection
  # -d run in background as a daemon, easier to use with opnx alias (without -d argument, terminal has to be closed manually when using opnx alias)
  # "${@}" pass all argument/s onto openpyn, double quotes to parse multi word string
  # -t 3 choose only 3 servers instead of default (10)
  # -m 40 reject all servers above specified load time
  sudo openpyn -fd "${@}" -t 3 -m 40 >/dev/null 2>&1

  if ! timing::wait_condition daemon_started 10000; then
    notification::error "Failed to execute openpyn as daemon."

    exit 1
  else
    notification::notify "Daemon started." "" "Establishing connection."
  fi

  if timing::wait_condition received_error 3500; then
    notification::error "$(get_error_message)" "" "Resetting back to default values."
    "${PROJECT_PATH}disconnection.sh"

    exit 1
  fi

  if timing::wait_condition is_connected 35000; then
    notification::notify "Connection established." "" "New IP: $(connection::get_ip)" "Country: $(get_country_name)" "Area: $(get_area_name)"
    "/home/${USER}/Documents/Gitlab Projects/mac-changer-simple/script.sh"

    exit 0
  elif authentication_failed; then
    for i in {0..2}; do
      if timing::wait_condition is_connected 10000; then
        notification::notify "Connection established." "" "New IP: $(connection::get_ip)" "Country: $(get_country_name)" "Area: $(get_area_name)"
        "/home/${USER}/Documents/Gitlab Projects/mac-changer-simple/script.sh"

        exit 0
      fi
      notification::error "AUTH_FAILLED. $((i + 1))/3"
    done
    notification::error "Failed to authenticate." "" "Resetting back to default values."
    "${PROJECT_PATH}disconnection.sh"
  else
    notification::error "Failed to connect." "" "Check \"systemctl status openpyn\" for more details."
  fi

  exit 1
}

main "${@}"
