#!/usr/bin/env bash

#######################################
# Get IP address.
# Arguments:
#   None
# Returns:
#   ip address
# Example:
#   readonly MY_IP=$(connection::get_ip)
#######################################
connection::get_ip() {

  curl --silent "ifconfig.me"
}
