#!/usr/bin/env bash

readonly PROJECT_PATH="/home/${USER}/Documents/Gitlab Projects/openpyn-simple-bash/"

load_packages() {
  for package in "notification" "timing" "openpyn_status" "connection"; do

    # shellcheck source=/dev/null
    source "${PROJECT_PATH}${package}.sh"
  done
}

is_connection_down() {

  local -r RESPONSE=$(openpyn_status::get_status)

  [[ "${RESPONSE}" == *"Active: inactive (dead)"* || "${RESPONSE}" == *"Active: failed"* ]]
}

main() {
  load_packages
  notification::notify "Disconnecting."
  openpyn -x >/dev/null 2>&1

  # wait for openpyn to kill connection
  if timing::wait_condition is_connection_down 8000; then
    notification::notify "Disconnected." "" "Default IP: $(connection::get_ip)."

    exit 0
  else
    notification::error "Failed to disconnect." "" "Check \"systemctl status openpyn\" for more details."

    exit 1
  fi
}

main "${@}"
