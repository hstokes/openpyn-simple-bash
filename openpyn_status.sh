#!/usr/bin/env bash

#######################################
# Get info about openpyn daemon.
# Arguments:
#   None
# Returns:
#   string
# Example:
#   readonly STATUS=$(openpyn_status::get_status)
#######################################
openpyn_status::get_status() {

  systemctl status openpyn
}

#######################################
# Check whether deamon info contains string.
# Arguments:
#   string
# Returns:
#   true if daemon info contains string, false otherwise
# Example:
#   if openpyn_status::status_contains "active (running)"; then echo "Daemon is running."; fi
#######################################
openpyn_status::status_contains() {

  [[ "$(openpyn_status::get_status)" == *"${*}"* ]]
}
