#!/usr/bin/env bash

#######################################
# Wait condition for dynamic programming.
# Arguments:
#   boolean function
#   time in millis
# Returns:
#   true if condition is met, false otherwise
# Example:
#   if timing::wait_condition is_directory_open 2000; then echo "Directory is open"; fi
#######################################
timing::wait_condition() {
  if [[ -z "${1}" ]]; then
    printf "%s\n" "[ $(date '+%d/%m/%Y %H:%M:%S') ] Function argument for wait_condition() is missing." >&2

    # bad argument
    exit 128
  elif [[ -z "${2}" ]]; then
    printf "%s\n" "[ $(date '+%d/%m/%Y %H:%M:%S') ] Timeout for wait_condition() is missing." >&2

    # bad argument
    exit 128
  fi

  local -r START=$(($(get_time_millis) + ${2}))

  while ! ${1}; do
    if [[ $(get_time_millis) -ge ${START} ]]; then

      return 1
    fi

    # 100 millis
    sleep 0.1
  done

  return 0
}

#######################################
# Get time in milliseconds.
# Arguments:
#   None
# Returns:
#   time in milliseconds
# Example:
#   local -r START=$(($(get_time_millis)))
#######################################
get_time_millis() {

  date +%s%3N
}
