<div align="center">

![alt text](img/info.png "openpyn-simple")

# Simple usage of openpyn.

</div>

### Disclaimer

Project is discontinued due to [python version of openpyn-simple](https://gitlab.com/hstokes/openpyn-simple-python).

Script contains absolute paths. Be aware to change it according to your system.

---

### Navigation

- [Features](#features)
- [Usage](#usage)
- [Examples](#examples)
- [Pro Tip](#pro-tip)
- [Development](#development)

---
### Features

- killswitch and running as a daemon by default
- disconnect from one location and connect to another one (openpyn can't handle that itself)
- fancy output
- mac-changer-simple integration (change MAC address on every successful connection)&ast;

&ast; requires [mac-changer-simple](https://gitlab.com/hstokes/mac-changer-simple)

---
### Usage

Run `./script.sh` with destination as a parameter. 

Parameter format is same as openpyn's except *-f*, *-d*, *-t 3* and *-m 40* arguments (are used by default). Check out [openpyn usage options](https://github.com/jotyGill/openpyn-nordvpn#usage-options).

---
### Examples

`./script.sh uk` 

`./script.sh Germany`

`./script.sh italy`

`./script.sh uk -a "'North West England'"`

`./denver`

Check out [examples folder](examples).

---
### Pro Tip

Check out [bashrc-aliases](https://gitlab.com/hstokes/bashrc-aliases) project for even better usage.

### Development

Check [bash-snippets](https://gitlab.com/hstokes/bash-snippets/snippets) for easier development.